from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Message
from .forms import MessageForm

# Create your views here.

def index(request):
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
            return HttpResponseRedirect('/')
    form = MessageForm()
    allmessage = Message.objects.all().order_by('-date')
    return render(request, 'tdd/home.html', {'form':form, 'allmessage':allmessage})