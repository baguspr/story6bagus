from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Message

# Create your tests here.

class TDDUnitTest(TestCase):
    def test_TDD_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_TDD_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'tdd/home.html')

    def test_TDD_landing_page_contains_hello_world(self):
        response = Client().get('/')
        self.assertContains(response, '<h1>Hello, World!</h1>', status_code=200)

    def test_TDD_can_create_new_activity(self):
        #Creating a new activity
        new_activity = Message.objects.create(activity='Hello World')

        #Retrieving all available acitivity
        counting_all_available_activity = Message.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_can_save_a_POST_request(self):
        response = Client().post('/', data={'activity': 'Hello World'})
        self.assertEqual(response.status_code, 302)

        new_response = Client().get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Hello World', html_response)