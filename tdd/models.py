from django.db import models

# Create your models here.
class Message(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    activity = models.CharField(max_length=100)
    
    def __str__(self):
        return '%s' % self.activity